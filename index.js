const botconfig = require("./botconfig.json");
const Discord = require("discord.js");
let gamenum = 0;
const money = require('discord-money');
const bot = new Discord.Client({
    disableEveryone: true
});
const fs = require("fs");
const ms = require("ms");
const db = require('quick.db')
const { Lavalink } = require('node-lavalink');


const voice = new Lavalink(lavalink);

voice.on('forwardWs', (serverId, data) => {
    if (bot.guilds.has(serverId)) bot.ws.connection.ws.send(data);
});

bot.on('raw', pk => {
    if (pk.t === 'VOICE_STATE_UPDATE') voice.voiceStateUpdate(pk.d);
    if (pk.t === 'VOICE_SERVER_UPDATE') voice.voiceServerUpdate(pk.d);
});

Number.prototype.pad = function(size) {
    var s = String(this);
    while (s.length < (size || 2)) {
        s = "0" + s;
    }
    return s;
};

bot.commands = new Discord.Collection();
fs.readdir("./cmds/", (err, files) => {
    console.log('Loading commands!')
    if (err) console.error(err);
    files.forEach((f, i) => {
        let props = require(`./cmds/${f}`);
        console.log(`${i +1}: ${f} loaded!`);
        bot.commands.set(props.help.name, props);
    });
});

bot.on("ready", () => {
    console.log('Floofy is online!')
    let game = setInterval(() => {
        let games = [""+bot.guilds.size+" guilds!", "/help", "The ping: "+Math.round(bot.ping)+"ms", ""+bot.guilds.map(g => g.memberCount).reduce((f, l) => f + l)+" users"];
        gamenum+=1;
        bot.user.setPresence({
          status: "online",
          game: {
            name: games[gamenum%games.length],
            type: 'WATCHING'
          }
        });
      }, 10000);
});

bot.on("message", async message => {
    if (message.channel.type === "dm") {
        let embed = new Discord.RichEmbed()
            .setTimestamp()
            .setTitle("Direct Message To The Bot")
            .addField(`Sent By:`, `<@${message.author.id}>`)
            .setColor("RANDOM")
            .setThumbnail(message.author.displayAvatarURL)
            .addField(`Message:`, `${message.content}`)
            .setFooter(`DM Bot Messages | DM Logs`)
        bot.users.get("286509757546758156").send(embed)
    }

    if (message.isMentioned(bot.user)) {
        message.channel.send("**My prefix is** \`/\`");
    };

    let prefix = botconfig.prefix;
    let sprefix = botconfig.sprefix;
    if (!message.content.startsWith(botconfig.prefix)) return;
    let messageArray = message.content.split(" ");
    let command = messageArray[0];
    let args = messageArray.slice(1);

    let cmd = bot.commands.get(command.slice(botconfig.prefix.length));
    if (cmd) cmd.run(bot, message, args);

});

bot.on("guildMemberAdd", (member) => {
    let dmMessage = db.get(`pjoinMessageDM_${member.guild.id}`);
    if (dmMessage) member.user.send(dmMessage).catch(() => {});
    let channelId = db.get(`pmessageChannel_${member.guild.id}`)
    if (!channelId) return
    let message = db.get(`pjoinMessage_${member.guild.id}`)
    if (!message) return
    message = message.replace(/{user}/g, `\`${member.user.username}#${member.user.discriminator}\``);
    message = message.replace(/{members}/g, member.guild.memberCount);
    bot.channels.get(channelId).send(message);
});

bot.on("guildMemberRemove", (member) => {
    let channelId = db.get(`pmessageChannel_${member.guild.id}`)
    if (!channelId) return
    let message = db.get(`pleaveMessage_${member.guild.id}`)
    if (!message) return
    message = message.replace(/{user}/g, `\`${member.user.username}#${member.user.discriminator}\``);
    message = message.replace(/{members}/g, member.guild.memberCount);
    bot.channels.get(channelId).send(message);
});

bot.login(botconfig.token);

module.exports = { voice };