const discord = require("discord.js")
const db = require("quick.db");
module.exports.run = (bot, message, args) => {
  let bUser = bot.users.get(args[0]); 
  let bChannel = message.guild.channels.get(db.get(`logchannel${message.guild.id}`));
  if(!message.member.hasPermission("BAN_MEMBERS")) return message.channel.send(`:x: **Missing Permission**: \`Ban Members\`.`);
  if(!args[0]) return message.channel.send(`:gear: **Usage:** \`/hackban <UserID>\` :gear:`);
  if (!bUser) {
    return message.channel.send(`:x: Failed to ban the ID: (\`${args[0]}\`) - (\`You typed the wrong ID, or the user is already banned\`)`);
  }
  let reply = args.join(" ").trim();
  bot.fetchUser(args[0]).then(user => {
      message.guild.ban(user.id).catch(err => {
          message.channel.send(`:x: Failed to ban user ` + user.id)
          console.log(err)
            }).catch(() => {
      })
      message.channel.send(`:white_check_mark: **Successfully** banned the user <@!${user.id}>.`)
      bChannel.send(`:warning: <@!${user.id}> (\`${user.id}\`) has been **Hackbanned** by \`${message.author.tag}\`.`);
    })
}


module.exports.help = {
  name: "hackban",
  desclist: "Ban a user that's not in the server",
  usage: "/hackban"
}