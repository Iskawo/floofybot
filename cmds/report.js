const Discord = require("discord.js");

module.exports.run = async (bot, message, args) => {
    let rUser = message.guild.member(message.mentions.users.first() || message.guild.members.get(args[0]));
    if(!rUser) return message.channel.send(":x: Please specify a valid \`User\`.");
    let rreason = args.join(" ").slice(22);

    let reportEmbed = new Discord.RichEmbed()
    .setDescription(`**__Floofy Report Logs__**`)
    .setColor("#15f153")
    .addField("Reported User", `${rUser} (**\`${rUser.id}\`**)`)
    .addField("Reported By", `${message.author} (**\`${message.author.id}\`**)\n`)
    .addField(`~~----------------------------------------------------~~`, `** **`)
    .addField("Reason", `**\`${rreason}\`**`);

    let reportschannel = message.guild.channels.find(`name`, "punishment-logs");
    if(!reportschannel) return message.channel.send(`:x: **Error**: \`punishment-logs\` channel not found.`);


    message.delete().catch(O_o=>{});
    reportschannel.send(reportEmbed);

    message.channel.send(`:white_check_mark: **${rUser}** Has been __reported__.`);

}
 
module.exports.help = {
  name: "report",
  desclist:"Report a user to staff.",
  usage:"/report <user> <reason>"
}