const Discord = require("discord.js");

module.exports.run = async (bot, message, args) => {
  function treatAsUTC(date) {
    var result = new Date(date);
    result.setMinutes(result.getMinutes() - result.getTimezoneOffset());
    return result;
}

function daysBetween(startDate, endDate) {
    var millisecondsPerDay = 24 * 60 * 60 * 1000;
    return (treatAsUTC(endDate) - treatAsUTC(startDate)) / millisecondsPerDay;
}

    const kkcuddle = bot.emojis.find("name", "kkcuddle");
    let sicon = message.guild.iconURL;
    let serverembed = new Discord.RichEmbed()
    .setDescription(`**Server Information:** \`${message.guild.name}\``)
    .setColor("RANDOM")
    .setThumbnail(sicon)
    .addField(":notepad_spiral: Server Name", `\`${message.guild.name}\``)
    .addField(":bust_in_silhouette: Owner", `${message.guild.owner}`)
    .addField(":date: Created On", `\`${message.guild.createdAt}\` \n(\`${Math.round(daysBetween(message.guild.createdAt, Date.now()))} Days ago\`)`)
    .addField(":earth_americas: Region", `\`${message.guild.region}\``)
    .addField(":loudspeaker: Notifications:", `\`${message.guild.defaultMessageNotifications}\``)
    .addField(":busts_in_silhouette: Total Members", `\`${message.guild.memberCount}\``)
    .addField(":speaker: Total Channels:", `\`${message.guild.channels.size}\``)
    .addField(":open_file_folder: Total Roles:", `\`${message.guild.roles.size}\``)
    .addField(kkcuddle + " Total Custom Emojis:", `\`${message.guild.emojis.size}\``)
    .addField(":link: Server Icon URL:", `**__${message.guild.iconURL}__**`)
    .setFooter(`Requested by: ${message.author.username}#${message.author.discriminator}`, icon = message.author.avatarURL)

    message.channel.send(serverembed);
}

module.exports.help = {
  name:"si",
  desclist: "Information on the server",
  usage: "/si"
}