const Discord = require("discord.js");

module.exports.run = async (bot, message, args) => {

let kiss = message.guild.member(message.mentions.users.first() || message.guild.members.get(args[0]));
const blowkiss = bot.emojis.find("name", "blowkiss");



let embed = new Discord.RichEmbed()
.setAuthor(message.author.username)
.setDescription("Usage for /kiss command:")
.addField(`**Usage**: \`/kiss @user / ID\``, blowkiss)
.setColor("#9B59B6");


if(!kiss) return message.channel.send(embed);
message.channel.send(`${message.author} has just kissed ${kiss}, how cute! ` + blowkiss, {
})


}
module.exports.help = {
    name: "kiss",
    desclist: "Kiss someone!",
    usage: "/kiss"
}