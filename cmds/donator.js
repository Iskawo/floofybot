const Discord = require('discord.js');
const fs = require("fs");
const dusers = JSON.parse(fs.readFileSync("./users.json", "utf8"));

module.exports.run = async (bot, message) => {
    if (message.guild.id !== "506105941498789888") {
        return message.channel.send(`This command can only be used in the Offical Floofy Server.`)
    }

    if (message.channel.id !== '508940730652164117') {
        return message.channel.send(`Please use the <#508940730652164117> channel.`)
    }

    let userID = message.author.id;
    if (message.author.id == dusers.donator) {
        return message.channel.send(`:x: You can't' run this command more than \`once\`.`)
    }

    dusers.donator.push(userID)
    var json = JSON.stringify(dusers);
    fs.writeFileSync("./users.json", json);
    message.channel.send(`<@!${message.author.id}>, :white_check_mark: **Thank you for your** \`support\`! You now have access to **donator** commands.`);
    let donatorrole = message.guild.roles.find(`name`, "Donator");
    await(message.member.addRole(donatorrole.id));
}
module.exports.help = {
    name: "donator",
    desclist: "Recieve your donator perks.",
    usage: "/donator"
}