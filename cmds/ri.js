const Discord = require('discord.js');
const config = require('../botconfig.json');

module.exports.run = (bot, message, args) => {


    function treatAsUTC(date) {
        var result = new Date(date);
        result.setMinutes(result.getMinutes() - result.getTimezoneOffset());
        return result;
    }
    
    function daysBetween(startDate, endDate) {
        var millisecondsPerDay = 24 * 60 * 60 * 1000;
        return (treatAsUTC(endDate) - treatAsUTC(startDate)) / millisecondsPerDay;
    }

    if (message.content.startsWith(config.prefix + 'ri')) {
        const name_id = args.slice(0).join(" ");
        const role_name = message.guild.roles.find(role => role.name === name_id);
        const role_id = message.guild.roles.find(role => role.id === name_id);
        if (role_id || role_name) {
            const role = role_id || role_name
            message.channel.send({embed: {
                color: 3447003,
                title: `:open_file_folder: Role: ${role.name}`,
                fields: [{
                    name: ":busts_in_silhouette: Members",
                    value: `\`\`${role.members.size}\`\``
                  },
                  {
                    name: ":radio_button: Hex",
                    value: `\`\`${role.hexColor}\`\``
                  },
                  {
                    name: ":date: Creation Date",
                    value: `\`\`${role.createdAt} (${Math.round(daysBetween(role.createdAt, Date.now()))} Days ago)\`\``
                  },
                  {
                    name: ":writing_hand: Editable",
                    value: `\`\`${role.editable}\`\``
                  },
                  {
                    name: ":shield: Managed",
                    value: `\`\`${role.managed}\`\``
                  },
                  {
                    name: ":id: ID",
                    value: `\`\`${role.id}\`\``
                  }
                ],
                timestamp: new Date(),
                footer: {
                  icon_url: message.author.avatarURL,
                  text: `Requested by: ${message.author.username}`
                }
              }
            });
        } else {
            message.channel.send(":x: **Correct Usage:** `/ri <@role> | <ID> | <name>`!");
        };
    };
}
module.exports.help = {
    name: 'ri',
    deslist: 'Displays information about a role.',
    usage: '/ri'
}
