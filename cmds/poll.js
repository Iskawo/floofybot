const Discord = require('discord.js');

exports.run = async (bot, message, args, ops) => {

    if (!message.member.hasPermission("MANAGE_MESSAGES")) {
        return message.channel.send(`:x: **Missing Permission**: \`Manage Messages\`.`);
	}
    
    if (!args[0]) return message.channel.send(`:x: \`Incorrect Usage\`. Correct usage: **/poll <question>**`);

    const embed = new Discord.RichEmbed()
        .setColor("#ff4646")
        .setFooter('React to Vote.')
        .setDescription(args.join(' '))
        .setTitle(`Poll Created By **\`${message.author.username}\`**`);
        
    let msg = await message.channel.send(embed)
        .then(function (msg) {
            msg.react("✅");
            msg.react("❎");
            message.delete({timeout: 1000});
            }).catch(function(error) {
            console.log(error);
            message.delete().catch(O_o=>{});
        });
};
module.exports.help = {
    name: "poll",
    desclist: "Create a poll.",
    usage: "/poll <question>"
}