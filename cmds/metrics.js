const Discord = require("discord.js")

module.exports.run = async (bot, message, args) => {
const moment = require("moment");
require("moment-duration-format");
let os = require('os')
let cpuStat = require("cpu-stat")

  let cpuLol;
  cpuStat.usagePercent(function(err, percent, seconds) {
    if (err) {
      return console.log(err);
    }




  const embedStats = new Discord.RichEmbed()
    .setTitle(`**\`Stats\`**`)
    .setColor("RANDOM")
    .addField(`\`•\` **Mem Usage**`, `\`${(process.memoryUsage().heapUsed / 1024 / 1024).toFixed(2)} / ${(os.totalmem() / 1024 / 1024).toFixed(2)} MB\``, true)
 
    .addField(`\`•\` **Users**`, `\`${bot.guilds.map(g => g.memberCount).reduce((f, l) => f + l)}\``, true)
    .addField(`\`•\` **Servers**`, `\`${bot.guilds.size.toLocaleString()}\``, true)
    .addField(`\`•\` **Channels**`, `\`${bot.channels.size.toLocaleString()}\``, true)
    .addField(`\`•\` **Ping**`, '' + `\`${Date.now() - message.createdTimestamp}\`` + ' \`ms\`', true)
    .addField(`\`•\` **Node**`, `\`${process.version}\``, true)
    .addField(`\`•\` **CPU**`, `\`\`\`md\n${os.cpus().map(i => `${i.model}`)[0]}\`\`\``)
    .addField(`\`•\` **CPU usage**`, `\`${percent.toFixed(2)}%\``,true)
    .addField(`\`•\` **Platform**`, `\`\`win ${os.arch()}\`\``,true)
    message.channel.send(embedStats)
  });


}

  module.exports.help = {
  name:"metrics",
  desclist:"Shows the bots metrics",
  usage:"/metrics"
}