 const Discord = require("discord.js");

module.exports.run = async (bot, message, args) => {

 let embed = new Discord.RichEmbed()
    .setAuthor(message.author.username)
    .setThumbnail(bot.user.avatarURL)
    .setDescription(`**__Premuim Commands__:**\n\n\`Shorten\` - **/shorten <URL> [title]** - Shortens the URL that you enter.\n\`Checklink\` - **/checklink <URL>** - Checks if the URL is safe or not.\n\`Hastebin\` - **/hastebin <text>** - Turns your text into a Hastebin file.\n\`Lingerie\` - **/lingerie**\n\`4k\` - **/4k**\n\`Porngif\` - **/porngif**\n\`Cosplay\` - **/cosplay**\n\`NSFW\` - **/nsfw**\n\n\`Floofy Bot\`:tm: ** ** ** ** • ** ** ** ** Hoppity Floof#6172`)
    .setColor("#9B59B6");
  message.channel.sendEmbed(embed);
}


module.exports.help = {
	name: "premium",
  desclist: "Shows whuch premium commands you can get with /donate",
  usage:"/premium"
}
