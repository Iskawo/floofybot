const Discord = require("discord.js");
module.exports.run = (bot, message, args) => {
    let reply = args.join(" ").trim();
    if (!reply)
    return message.channel.send("❌ **Incorrect Usage.** Try: \`/say <message>\`");
    if(!message.member.hasPermission("MANAGE_MESSAGES")) return message.channel.send("❌ **Missing Permission**: \`Manage Messages\`.");
    else {
    message.channel.send(args.join(" "));
    message.delete().catch(O_o=>{});
 }
};

module.exports.help = {
    name: "say",
    description: "Makes the bot repeat your message.",
    usage: "/say [message]"
};