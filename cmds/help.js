const discord = require("discord.js");

module.exports.run = async (bot, message, args) => {

        let embed =  new discord.RichEmbed(), cmd, description, examples;
        if (args.length === 0) {
            embed = new discord.RichEmbed()
                .setAuthor("FloofyBot help", icon = bot.user.avatarURL)
                .setColor("PURPLE")
                .addField("Our Support Server", "(https://discord.gg/4K4DYM5)", true)
                .addField("Invite Link", ":link: (https://goo.gl/fz51Yk) :link:", true)
                .addField("Commands List", ":pencil2: **Type** \`/commands\` :pencil2:", true)
                .addField("Settings Command:", ":gear: **Type** \`/settings\` :gear:", true);
            return message.channel.send({embed});
        }

        cmd = args[0].replace("/", "");

        embed.setTitle(':bulb: Info on command /' + cmd)
            .setColor(0xFFFFFF)
            .addField("Description", description)
            .addField("Example", examples);

        return message.channel.send({embed});
    }
module.exports.help = {
    name: 'help',
    deslist: 'Displays help.',
    usage: '/help'
}