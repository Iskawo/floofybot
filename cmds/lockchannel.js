const Discord = require("discord.js");
const ms = require("ms");
module.exports.run = async (bot, message, args) => {
    if (!message.member.hasPermission("MANAGE_MESSAGES")) {
        return message.channel.send(":x: **Missing Permission**: \`Manage Messages\`.");
    }

    if (message.member.hasPermission("MANAGE_MESSAGES")) {
        if (!bot.lockit) bot.lockit = [];
        let time = args.join(' ');
        let validUnlocks = ['release', 'unlock'];
        if (validUnlocks.includes(time)) {
            message.channel.overwritePermissions(message.guild.id, {
                SEND_MESSAGES: null
            }).then(() => {
                delete bot.lockit[message.channel.id];
            }).catch(error => {
                console.log(error);
            });
        } else {
            message.channel.overwritePermissions(message.guild.id, {
                SEND_MESSAGES: false
            }).then(() => {
                message.channel.sendMessage(':warning: Channel locked down by \`' + message.author.username + '\`') 

                bot.lockit[message.channel.id] = setTimeout(() => {
                    message.channel.overwritePermissions(message.guild.id, {
                        SEND_MESSAGES: null
                    })
                    delete bot.lockit[message.channel.id];
                }, ms(time));

            }).catch(error => {
                console.log(error);
            });
        }
    }
}
module.exports.help = {
    name: "lockchannel",
    desclist: "lockdown a channel",
    usage: "/lockchannel"
}