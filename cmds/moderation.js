const Discord = require("discord.js");

module.exports.run = async (bot, message, args) => {

        message.channel.send("**Commands for** \`/moderation\`.\n\n\`Ban\` - **/ban <@user> <reason>** - Bans a User.\n\`Unban\` - **/unban <UserID>** - Unbans a User.\n\`Hackban\` - **/hackban <UserID>** - Bans a User who is not in the server.\n\`Kick\` - **/kick <user>** - Kicks a User.\n\`Permmute\` - **/permmute <@user>** - Permanently mutes a user.\n\`Mute\` - **/mute <user> <time>** - Mutes a User.\n\`Unmute\` - **/unmute <@user>** - Unmutes a muted user.\n\`Warn\` - **/warn <@user> <reason>** - Warns a user.\n\`CreateRole\` - **/createrole <RoleName>** - Creates a role in the server.\n\`DeleteRole\` - **/deleterole <RoleName>** - Deletes a role from the server.\n\`Lockdown\` - **/lockdown <time>** - Locks the chat down.\n\`Lockchannel\` - **/lockchannel** - Permanently locks down a chat.\n\`Purge\` - **/purge <amount>** - Purges messages from the chat.\n\`Report\` - **/report <user> <reason>** - Report a User.\n\`Removerole\` - **/removerole <@user> <RoleName>** - Removes a role from a user.\n\`Clearchat\` - **/clearchat** - Clears the chat.\n\`Poll\` - **/poll <question>** - Creates a poll.\n\`Rolecolour\` - **/rolecolour <@role> <new HEX colour>** - Changes the colour of a role.")
    };
module.exports.help = {
    name: 'moderation',
    deslist: 'Displays the lis of commands for the section Moderation',
    usage: '/moderation'
}