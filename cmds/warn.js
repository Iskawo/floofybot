const Discord = require('discord.js');
const db = require("quick.db");
const send = require('quick.hook');

module.exports.run = (bot, message, args) => {
    let messageArray = message.content.split(" ");
    let reason = args.slice(1).join(' ');
    let user = message.mentions.users.first();
    let warnChannel = message.guild.channels.get(db.get(`logchannel${message.guild.id}`));
    if (!warnChannel) return message.channel.send(`:x: **Couldn't find the** \`punishment-logs\` **channel**!`)
    if (message.mentions.users.size < 1) return message.reply(':x: You must mention a valid \`User\` to warn.').catch(console.error);
    if (reason.length < 1) return message.reply(':x: You must supply a \`reason\` for the warning.');
    if (!message.member.hasPermission("MUTE_MEMBERS")) return message.channel.send(":x: **Missing Permission**: \`Manage Messages\`.");
    let mentionedUser = message.guild.member( message.mentions.users.first());
    if (!mentionedUser) return message.reply("mention user");
    if (mentionedUser.hasPermission("ADMINISTRATOR")) return message.reply(":x: Sorry, This user can't be \`warned\`.");
    warnChannel.send({embed: {
        color: 16381497,
        author: {
        name: `Floofy Warn Logs`,
        icon_url: user.avatarURL
        },
        title: `**Warning** issued to \`${user.username}\` (\`${user.id}\`)`,
        url: "",
        description: "",
         fields: [{
             name: "**Staff Member**:",
             value: `\n__${message.author.username}__ (\`${message.author.id}\`)\n`
         },
         {
             name: "**Reason**:",
            value: `\`${reason}\``
         }
        ],
        // {
        //     name: "Markdown",
        //     value: "You can put all the *usual* **__Markdown__** inside of them."
        // }
        // ],
        timestamp: new Date(),
        footer: {
        icon_url: bot.user.avatarURL,
        text: "Warnings are not currently tracked"
        }
    }
});
message.channel.send(`:white_check_mark: **${user.username}** Has been __warned__ for "${reason}"`);
user.send(`:warning: You have been warned in \`${message.guild.name}\` for the reason "\`${reason}\`". Please ensure you follow all rules that apply to you in the server/guild as being warned multiple times can lead to you being banned or kicked.`);
};

module.exports.help = {
    name: 'warn',
    description: 'Issues a warning to the mentioned user.',
    usage: '/warn [user] [reason]'
};