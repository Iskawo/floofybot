const Discord = require('discord.js');
const moment = require("moment");

module.exports.run = async (bot, message, args) => {
	
	function treatAsUTC(date) {
        var result = new Date(date);
        result.setMinutes(result.getMinutes() - result.getTimezoneOffset());
        return result;
    }
    
    function daysBetween(startDate, endDate) {
        var millisecondsPerDay = 24 * 60 * 60 * 1000;
        return (treatAsUTC(endDate) - treatAsUTC(startDate)) / millisecondsPerDay;
    }

	let user;
    if (message.mentions.users.first()) {
      user = message.mentions.users.first();
    } else {
        user = message.author;
    }
    const member = message.guild.member(user);
	
    const embed = new Discord.RichEmbed()
		.setColor('RANDOM')
		.setThumbnail(user.avatarURL)
		.setTitle(`**User Info:** \`${user.username}#${user.discriminator}\``)
		.addField(":id: ID:", `\`${user.id}\``, true)
		.addField(":pencil2: Nickname:", `${member.nickname !== null ? `\`${member.nickname}\`` : `\`None\``}`, true)
		.addField(":calling: Account Created:", `(\`${Math.round(daysBetween(user.createdAt, Date.now()))} Days ago\`)`, true)
		.addField(":date: Joined Server:", `(\`${Math.round(daysBetween(member.joinedAt, Date.now()))} Days ago\`)`, true)
		.addField(":robot: Bot:", `\`${user.bot}\``, true)
		.addField(":joystick: Status:", `\`${user.presence.status}\``, true)
		.addField(":video_game: Playing:", `${user.presence.game ? user.presence.game.name : `\`None\``}`, true)
		.addField(":open_file_folder: Roles:", member.roles.map(roles => `\`${roles.name}\``).join('**|** '), true)
		.setFooter(`Requested by: ${message.author.username}#${message.author.discriminator}`, icon = message.author.avatarURL)
     message.channel.send({embed});
    }
module.exports.help = {
    name: "userinfo",
    desclist: "Shows information about a user.",
    usage: "/userinfo"
}