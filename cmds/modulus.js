const Discord = require("discord.js")
exports.run = (client, message, args) => {
    let numArray = args.map(n => parseInt(n));
    let total = numArray.reduce( (p, c) => p%c);

    message.channel.send(total);
};
module.exports.help = {
    name: "modulus",
    desclist: "Returns the absolute value of the specified number.",
    usage: "modulus [num]"
};