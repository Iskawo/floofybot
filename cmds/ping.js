const Discord = require("discord.js");

module.exports.run = async (bot, message, args) => {
    var embedping = new Discord.RichEmbed()
       .setDescription(':ping_pong: Your ping is `' + `${Date.now() - message.createdTimestamp}` + ' ms`', "")
       .setColor(0xff8000)
       message.channel.send(embedping);

}





module.exports.help = {
    name: "ping",
    desclist: "Use to see the ping of the bot",
    usage:"/ping"
}