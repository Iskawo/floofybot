const Discord = require("discord.js");
const db = require("quick.db");
const send = require('quick.hook');

module.exports.run = async (bot, message, args) => {
   
    let bUser = message.guild.member(message.mentions.users.first() || message.guild.members.get(args[0]));
    if(!bUser) return message.channel.send(":x: Please specify a valid \`User\`.");
    let bReason = message.content.split(" ").slice(2).join(" ");
    if(!bReason) return message.channel.send(":x: Please specify a reason for this ban.");
    if(!message.member.hasPermission("BAN_MEMBERS")) return message.channel.send(":x: **Missing Permission**: \`Ban Members\`.");
    if(bUser.hasPermission("ADMINISTRATOR")) return message.channel.send(":x: Sorry, This user can't be \`Banned\`.");
    let banChannel = message.guild.channels.get(db.get(`logchannel${message.guild.id}`));
    if(!banChannel) return message.channel.send(":x: Can't find \`#punishment-logs\` channel.");
    message.channel.send(`:white_check_mark: **Success!** ${bUser} (\`${bUser.id}\`) has been banned. Reason: \`${bReason}\``);

    await bUser.send(`:warning: You have been \`Banned\` in \`${message.guild.name}\`.`);
    message.guild.member(bUser).ban(bReason);
    banChannel.send(`:warning: ${bUser} (\`${bUser.id}\`) has been **Banned** by \`${message.author.tag}\`  For \`${bReason}\``);
}

module.exports.help = {
  name:"ban",
  desclist:"ban a user",
  usage:"/ban"
}