const Discord = require("discord.js");

module.exports.run = async (bot, message, args) => {

        message.channel.send("**Commands for** \`/miscellaneous\`.\n\n\`8ball\` - **/8ball <question>** - Ask 8ball a question.\n\`Quiz\` - **/quiz** - Asks you a quiz question.\n\`Poll\` - **/poll <question>** - Creates a poll.\n\`Avatar\` **/avatar** - Sends your avatar in chat.\n\`Emojilist\` - **/emojilist** - Shows the servers custom emojis.\n\`Say\` - **/say <message>** - Gets the bot to repeat your message.\n\`Smokeweed\` - **/smokeweed** - Smoke some weed!\n\`Ascii\` - **/ascii <YourWordHere>** - Turns your word into Ascii.\n\`Lick\` - **/lick <@user>** - Lick a User.\n\`Kiss\` - **/kiss <@user/ID>** - Kiss someone.\n\`Love\` - **/love <@user>** - Loves a User.\n\`Hug\` - **/hug <@user>** - Hugs a User.\n\`Pat\` - **/pat <@user>** - Pats a User\n\`RKO\` - **/rko <@user>** - RKO Someone!\n\`Server Info\` - **/serverinfo** - Shows information about the server.\n\`Bot Info\` - **/botinfo** - Shows information about the bot.\n\`Ping\` - **/ping** - Check the bots Ping.\n\`Stats\` - **/stats** - Check the bots stats.\n\`Duck\` - **/duck** - Show an image of a duck.\n\`Cookie\` - **/cookie** - Shows an image of some cookies.\n\`Potato\` - **/potato** - Shows an image of some potatoes.\n\`Add\` - **/add <numbers>** - Adds the numbers together and outputs the result.\n\`Subtract\` - **/subtract <numbers>** - Subtracts the numbers and outputs the result.\n\`Divide\` - **/divide <numbers>** - Divides the numbers and outputs the result.\n\`Modulus\` - **/modulus <numbers>** - Devides the numbers and outputs remainder.\n\`Gay\` - **/gay** - See how gay you are.")
    };
module.exports.help = {
    name: 'miscellaneous',
    deslist: 'Displays the lis of commands for the section Miscellaneous',
    usage: '/miscellaneous'
}