const Discord = require("discord.js")
const { voice: link } = require('../index.js');

module.exports.run = async (bot, message, args) => {
    const player = link.players.get(message.guild.id);
    player.setVolume(args.slice(0).join(" "));
    message.channel.send(`:loudspeaker: **Volume Changed to:** \`\`${player.volume}%\`\``);
}

module.exports.help = {
    name:"volume",
    desclist:"Change the volume of a song.",
    usage:"/volume"
  }