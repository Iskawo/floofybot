const Discord = require("discord.js");
const db = require("quick.db");
const send = require('quick.hook');

module.exports.run = async (bot, message, args) => {
    let kick = message.guild.member(message.mentions.users.first() || message.guild.members.get(args[0]));
    let kReason = message.content.split(" ").slice(2).join(" ");
    let kickChannel = message.guild.channels.get(db.get(`logchannel${message.guild.id}`));

    if(!kick) return message.channel.send(":x: Please specify a valid \`User\`.");
    if(!kReason) return message.channel.send(":x: Please specify a reason for this kick.");
    if(!message.member.hasPermission("KICK_MEMBERS")) return message.channel.send(":x: **Missing Permission**: \`Kick Members\`.");
    if(!kick.kickable) return message.channel.send(":x: Sorry, This user can't be \`Kicked\`.");
    message.channel.send(`:white_check_mark: **Success!** ${kick} (\`${kick.id}\`) has been kicked. Reason: \`${kReason}\``);
      await kick.send(`:warning: ${kick}, You have been \`Kicked\` from \`${message.guild.name}\` for the reason "\`${kReason}\`".`);
    message.guild.member(kick).kick(kReason);
    kickChannel.send(`**User** \`>\` ${kick} (\`${kick.id}\`) has been kicked by \`${message.author.tag}\` for \`${kReason}\``);
    
}

module.exports.help = {
  name:"kick",
  desclist:"Kick a user",
  usage:"/kick <@user> <reason>"
}