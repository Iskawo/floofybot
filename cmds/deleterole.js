const Discord = require('discord.js');
const config = require('../botconfig.json');

module.exports.run = (bot, message, args) => {
   if (message.content.startsWith(config.prefix + 'deleterole')) {
    const name = args.slice(0).join(" ");
    if(!message.member.hasPermission("MANAGE_ROLES")) return message.channel.send(":x: **Missing Permission**: \`Manage Roles\`.");
    const role = message.guild.roles.find(role => role.name == name);
    if(!role) return message.channel.send(":x: Please specify a valid \`Role\`.");
    if (!name) {
        message.channel.send(`:x: __**Incorrect Usage**__. **Correct Usage:** \`/deleterole <rolename>\``)
    } else {
        role.delete();
        message.channel.send(`\*\*:white_check_mark: **Success!** __Role__: \`\`${name}\`\` has been __deleted__\*\*`);
        };
    };
}
module.exports.help = {
    name: "deleterole",
    desclist: "Deletes a role from the server",
    usage: "/deleterole <rolename>"
}