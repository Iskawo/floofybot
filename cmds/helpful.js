const Discord = require("discord.js");

module.exports.run = async (bot, message, args) => {

        message.channel.send("**Commands for** \`/helpful\`.\n\n\`Help\` - **/help** - Get help.\n\`Commands\` - **/commands** - Get a list of commands.\n\`/settings\` - **/settings** - Get the list of settings on the server.\n\`Uptime\` - **/uptime** - Shows the bots uptime.\n\`Donate\` - **/donate** - Gets the bots donation link.\n\`Donator\` - **/donator** - Claim your donator perks.\n\`Premium\` - **/premium** - Shows the current premium features.\n\`Metrics\` - **/metrics** - Shows the bots metrics.\n\`Userinfo/Ui\` - **/userinfo | /ui** - Gets information about you/user.\n\`Roleinfo/Ri\` - **/roleinfo | /ri** - Shows information about a role.\n\`Invite\` - **/invite** - Get the link to invite the bot.\n\`Support\` - **/support** - Need support? Use this command,\n\`Or Click Here\`: \`\`\`py\n-=- https://discord.gg/4K4DYM5 -=-\`\`\`")
    };
module.exports.help = {
    name: 'helpful',
    deslist: 'Displays the list of commands for the section Helpful',
    usage: '/helpful'
}