const Discord = require('discord.js');
const request = require('snekfetch');
const fs = require("fs")
const randomPuppy = require('random-puppy');
const dusers = JSON.parse(fs.readFileSync("./users.json", "utf8"));

module.exports.run = async (bot, message) => {
    if (!dusers.donator.includes(message.author.id)) return message.channel.send(":x: You must **\`donate\`** to use that feature. \`/donate\`");
    if (!message.channel.nsfw) return message.channel.send(":underage: NSFW Command. Please switch to **\`NSFW channel\`** in order to use this command.")

    let subreddits = [
        'NSFW_Wallpapers',
        'SexyWallpapers',
        'HighResNSFW',
        'nsfw_hd',
        'UHDnsfw'
    ]
    let sub = subreddits[Math.round(Math.random() * (subreddits.length - 1))];

    randomPuppy(sub)
        .then(url => {
            const embed = new Discord.RichEmbed()
                .setColor("RANDOM")
                .setAuthor("Floofy Bot", bot.user.avatarURL)
                .setFooter("4k Image")
                .setImage(url);
            message.channel.send({
                embed
            });
        })
}
module.exports.help = {
    name: "4k",
    desclist: "Shows a 4k Image.",
    usage: "/4k"
}