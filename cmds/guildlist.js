const Discord = require("discord.js");

module.exports.run = async (bot, message, args) => {
    if (message.author.id == "286509757546758156") {
        const guilds = bot.guilds.map(g => g.name)
        message.channel.send({embed: {
            title: "Guild List",
            color: 0x00AE86,
            description: `\`\`\`${guilds}\`\`\`\n\n`,
            footer: {text: "Requested by: " + message.author.username, icon_url: message.author.avatarURL}
        }});
    } else {
        message.channel.send('\*\*:x: Only \`\`Hoppity Floof\`\` can use this command.\*\*');
    };
}
module.exports.help = {
    name: "guildlist",
    desclist: "Lists the server's that the bot is in",
    usage: "/guildlist"
}