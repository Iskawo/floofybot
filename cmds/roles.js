const discord = require("discord.js");

module.exports.run = async (bot, message, args) => {
let sicon = (`${message.guild.iconURL}`)
let roles = (message.guild.roles.map(roles => `\`${roles.name}\``).join(' **|** '))
let rolesembed = new discord.RichEmbed()
.setDescription(`:notepad_spiral: **Server Name** \`>\` \`${message.guild.name}\`\n\n:open_file_folder: Total Server Roles: \`${message.guild.roles.size}\`\n\n${roles}`)
.setColor("RANDOM")
.setThumbnail(sicon)
.setFooter(`${message.author.username}`, icon = message.author.avatarURL)
message.channel.send(rolesembed)
}
module.exports.help = {
    name: "roles"
}