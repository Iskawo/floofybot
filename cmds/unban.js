const discord = require("discord.js");
const db = require("quick.db");
const send = require('quick.hook');

module.exports.run = (bot, message, args) => {
  let aUser = message.guild.member(message.mentions.users.first());
    if (!message.member.hasPermission("BAN_MEMBERS")) return message.channel.send(":x: **Missing Permission**: \`Ban Members\`.");
    let unbanChannel = message.guild.channels.get(db.get(`logchannel${message.guild.id}`));
    bot.unbanAuth = message.author;
    let userid = args[0];
    if (!userid) return message.channel.send(":x: Please specify a valid \`User\`.").catch(console.error);
    bot.fetchUser(userid).then(id => {
    message.guild.unban(id);
    message.channel.send(`:white_check_mark: **Success!** \`${id.username}\` (\`${userid}\`) Has been __**unbanned**__.`);
    unbanChannel.send(`:warning: ${id} (\`${userid}\`) has been **unbanned** by \`${message.author.tag}\``);
  })
}
module.exports.help = {
    name: "unban",
    desclist: "Unban a user",
    usage: "/unban"
}