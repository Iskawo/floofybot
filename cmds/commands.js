const Discord = require("discord.js");

module.exports.run = async (bot, message, args) => {
        
        let embed = new Discord.RichEmbed(), cmds, description;

        description = [
            "**/moderation** : \`Displays commands for server management.\`",
            "**/miscellaneous** : \`Displays random fun commands!\`",
            "**/helpful** : \`Displays helpful commands.\`",
            "**/premium** : \`Displays donator commands.\`"
        ];

        description.forEach(item => {
            let name, desc;
            name = item.split(' : ')[0];
            desc = item.split(' : ')[1];
            embed.addField(name, desc);
        });

        embed.setColor(0x32363B)
            .setAuthor("Commands", url = bot.user.avatarURL)
            .setThumbnail(bot.user.avatarURL)
            .setDescription("Type **\`/help <Command name>\`** for more info on the command with examples!")
            .setColor("#9B59B6");           

        return message.channel.send({embed});

    }
module.exports.help = {
    name: 'commands',
    deslist: 'Displays all the available commands',
    usage: '/commands'
}