const Discord = require("discord.js");

module.exports.run = async(bot, message, Discord) => {
let args = message.content.split(" ").slice(1);
if (!args[0]) return message.channel.send(`:x: \`Incorrect Usage\`. Correct usage: **/rolecolour <@role> <#HEX>**`);
if(!message.member.hasPermission("MANAGE_ROLES")) return message.reply(`:x: **Missing Permission**: \`Manage Messages\`.`).then(msg => msg.delete(6000));
let role = message.mentions.roles.first() || message.guild.roles.find('name', args[0]);
if(!role) return message.channel.send(`:x: **Please mention a valid** **\`Role\`**`);

let color = args.slice(1).join(" ");
if(!color) return message.channel.send(`:x: **Please send a valid** **\`HEX Code\`**`);

await role.setColor(color).catch(error => message.channel.send(`Error: ${error}`));
await message.channel.send(`\`${role.name}\`'s colour was changed to ${role.color}`).catch(error => message.channel.send(`Error: ${error}`));
    };
module.exports.help = {
    name: "rolecolour",
    desclist: "Changes the colour of a role.",
    usage:"/rolecolour <role> <newcolour>"
}