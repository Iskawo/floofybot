const Discord = require("discord.js");

module.exports.run = async (bot, message, args) => {
    let answer = args.join(" ").trim();
    let random = [
        "No",
        "Maybe",
        "Yes",
        "Possibly",
        "I have no idea",
        "Who knows...",
        "I don't think so",
        "Definitely",
        "You tell me",
        "Only time will tell"
    ];
    let randomresult = Math.floor((Math.random() * random.length));
    if (!answer)
        return message.channel.send("❌ **Incorrect Usage.** Try: \`/8ball <question>\`");
    else {
        return message.channel.send(random[randomresult]);
    }
}
module.exports.help = {
    name: "8ball",
    desclist: "Ask 8ball a question.",
    usage: "/8ball <question>"
}