const Discord = require("discord.js");

module.exports.run = async (bot, message, args) => {

        message.channel.send("**Commands for** \`/music\`.\n\n\`Join/connect\` - **/join** or **/summon** - Summons the bot into the VC you are in.\n\`Play\` - **/play <SongName/Link>** or **/p <SongName/Link>** - Plays the song you entered.\n\`Leave\` - **/leave** - Gets the bot to leave the VC it's in.\n\`Skip\` - **/skip** - Skips the song that is currently playing.\n\`Volume\` - **/volume <NewVolume>** - Changes the volume of the bot.")
    };
module.exports.help = {
    name: 'music'
}