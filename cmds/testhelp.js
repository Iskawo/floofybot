const Discord = require("discord.js");
module.exports.run = (bot, message, args) => {
let pages = ['**__Page 1__**\n\n__\`Moderation\`__ \n\`Ban\` - **/ban <@user> <reason>** - Bans a User.\n\`Unban\` - **/unban <UserID>** - Unbans a User.\n\`Hackban\` - **/hackban <UserID>** - Bans a User who is not in the server.\n\`Kick\` - **/kick <user>** - Kicks a User.\n\`Permmute\` - **/permmute <@user>** - Permanently mutes a user.\n\`Mute\` - **/mute <user> <time>** - Mutes a User.\n\`Unmute\` - **/unmute <@user>** - Unmutes a muted user.\n\`Warn\` - **/warn <@user> <reason>** - Warns a user\n\`CreateRole\` - **/createrole <RoleName>** - Creates a role in the server.\n\`DeleteRole\` - **/deleterole <RoleName>** - Deletes a role from the server.\n\`Lockdown\` - **/lockdown <time>** - Locks the chat down.\n\`Lockchannel\` - **/lockchannel** - Permanently locks down a chat.\n\`Purge\` - **/purge <amount>** - Purges messages from the chat.\n\`Report\` - **/report <user> <reason>** - Report a User.\n\`Removerole\` - **/removerole <@user> <RoleName>** - Removes a role from a user\n\`Clearchat\` - **/clearchat** - Clears the chat.\n\`Poll\` - **/poll <question>** - Creates a poll.\n\`Rolecolour\` - **/rolecolour <@role> <new HEX colour>** - Changes the colour of a role.', '**__Page 2__**\n\n__\`Miscellaneous\`__\n\`Avatar\` - **/avatar** - Sends your avatar in chat.\n\`Emojilist\` - **/emojilist** - Shows the servers custom emojis.\n\`Say\` - **/say <message>** - Gets the bot to repeat your message.\n\`Lick\` - **/lick <@user>** - Lick a User.\n\`Kiss\` - **/kiss <@user/ID>** - Kiss someone.\n\`Love\` - **/love <@user>** - Loves a User.\n\`Hug\` - **/hug <@user>** - Hugs a User.\n\`Pat\` - **/pat <@user>** - Pats a User\n\`RKO\` - **/rko <@user>** - RKO Someone!\n\`Server Info\` - **/serverinfo** - Shows information about the server.\n\`Bot Info\` - **/botinfo** - Shows information about the bot.\n\`Ping\` - **/ping** - Check the bots Ping.\n\`Stats\` - **/stats** - Check the bots stats.\n\`Duck\` - **/duck** - Show an image of a duck.\n\`Cookie\` - **/cookie** - Shows an image of some cookies.\n\`Potato\` - **/potato** - Shows an image of some potatoes.\n\`Add\` - **/add <numbers>** - Adds the numbers together and outputs the result.\n\`Subtract\` - **/subtract <numbers>** - Subtracts the numbers and outputs the result.\n\`Divide\` - **/divide <numbers>** - Divides the numbers and outputs the result.\n\`Modulus\` - **/modulus <numbers>** - Devides the numbers and outputs remainder.', '**__Page 3__**\n\n__\`Donator\`\n__\`NSFW Commands:\`__\n\`4k\` - **/4k** - Shows a 4k image.\n\`NFSW\` - **/nsfw** - General NSFW Images.\n\`Porngif\` - **/porngif** - Shows a porn GIF.\n\`Lingerie\` - **/lingerie** - Show an image of Lingerie.\n\`Cosplay\` - **/cosplay** - Shows a cosplay image.', '**__Page 4__**\n\n__\`Helpful\`__\n\`Help\` - **/help** - Get a list of commands\n\`Donate\` - **/donate** - Gets the bots donation link.\n\`Shorten\` - **/shorten <URL> [title]** - Shortens the URL that you enter.\n\`Checklink\` - **/checklink <URL>** - Checks if the URL is safe or not.\n\`Donator\` - **/donator** - Claim your donator perks.\n\`Premium\` - **/premium** - Shows the current premium features.\n\`Metrics\` - **/metrics** - Shows the bots metrics.\n\`Userinfo/Ui\` - **/userinfo | /ui** - Gets information about you/user.\n\`Roleinfo/Ri\` - **/roleinfo | /ri** - Shows information about a role.\n\`Invite\` - **/invite** - Get the link to invite the bot.\n\`Support\` - **/support** - Need support? Use this command,\nOr Click Here: https://discord.gg/4K4DYM5']; 
let page = 1; 
const embed = new Discord.RichEmbed() 
  .setColor(0xffffff)
  .setFooter(`Page ${page} of ${pages.length}`) 
  .setDescription(pages[page-1])

message.channel.send(embed).then(message => { 
 
  message.react('⏪').then( r => { 
    message.react('⏩') 
   
    const backwardsFilter = (reaction, user) => reaction.emoji.name === '⏪' && user.id === message.author.id;
    const forwardsFilter = (reaction, user) => reaction.emoji.name === '⏩' && user.id === message.author.id; 
   
    const backwards = message.createReactionCollector(backwardsFilter, { time: 60000 }); 
    const forwards = message.createReactionCollector(forwardsFilter, { time: 60000 }); 
   
    
    backwards.on('collect', r => { 
      if (page === 1) return; 
      page--; 
      embed.setDescription(pages[page-1]); 
      embed.setFooter(`Page ${page} of ${pages.length}`); 
      message.edit(embed) 
    })
   
    forwards.on('collect', r => { 
      if (page === pages.length) return; 
      page++; 
      embed.setDescription(pages[page-1]); 
      embed.setFooter(`Page ${page} of ${pages.length}`); 
      message.edit(embed) 
    })
 
        })
    })
}
module.exports.help = {
    name: 'testhelp',
    deslist: 'Displays all the available commands for your permission level.',
    usage: '/testhelp'
}