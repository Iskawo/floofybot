const Discord = require("discord.js");

module.exports.run = async (bot, message, args) => {


let love = message.guild.member(message.mentions.users.first() || message.guild.members.get(args[0]));
const kkcuddle = bot.emojis.find("name", "kkcuddle");


let embed = new Discord.RichEmbed()
.setAuthor(message.author.username)
.setDescription("Usage for /Love command:")
.addField(`**Usage**: \`/love @user\``, kkcuddle)
.setColor("#9B59B6");



if(!love) return message.channel.send(embed);
message.channel.send(`${message.author} gave ${love} some love!` + kkcuddle, {
})


}
module.exports.help = {
    name: "love",
    desclist: "Love someone!",
    usage: "/love"
}