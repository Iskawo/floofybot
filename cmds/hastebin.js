const Discord = require('discord.js')
const snekfetch = require('snekfetch');
const fs = require("fs")
const dusers = JSON.parse(fs.readFileSync("./users.json", "utf8"));
module.exports.run = async (bot, message, args) => {
	if (!dusers.donator.includes(message.author.id)) return message.channel.send(":x: You must **\`donate\`** to use that feature. \`/donate\`");
	if (!args.slice(0)
		.join(' ')) return message.channel.send(':x: **Provide some text!** __**Usage**__: \`/hastebin <text>\`');
	snekfetch.post('https://hastebin.com/documents', {headers: {}})
		.send(args.slice(0)
			.join(' '))
		.then(body => {
			message.channel.send(`:white_check_mark: **Successfully posted text to** \`Hastebin\`\nURL: https://hastebin.com/` + body.body.key);
			message.delete().catch(O_o=>{});
        });
    }
module.exports.help = {
    name: "hastebin",
    desclist: "Convert your inputted words to a Hastebin file.",
    usage: "/hastebin <text>"
}