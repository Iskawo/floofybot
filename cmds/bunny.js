const Discord = require("discord.js");
const client = new Discord.Client();
const randomPuppy = require('random-puppy');

exports.run = async (bot, message, args, level) => {

  let subreddits = [
      'Rabbits',
      'cutebunnies',
      'bunnygifs',
      'BunniesStandingUp'
]

let sub = subreddits[Math.round(Math.random() * (subreddits.length - 1))];

randomPuppy(sub)
.then(url => {
    const embed = new Discord.RichEmbed()
        .setColor("RANDOM")
        .setAuthor("Floofy Bot", bot.user.avatarURL)
        .setFooter("Bunnies Are Cute")
        .setImage(url);
    message.channel.send({
        embed
    });
})
}

  if(client.user && message.content === "undefined") {
      message.delete()
  }
module.exports.help = {
  name: "bunny",
  desclist: "Shows cute images of bunnies!",
  usage: "bunny"
}