const Discord = require("discord.js");
module.exports.run = (client, message) => {
    message.channel.send(`:gear: **Want to \`donate\` and receive the bot's donator features?** :gear:\nClick the link below to donate through \`PayPal\`\n:link: **__https://donatebot.io/checkout/506105941498789888__** :link:`);
    };

    module.exports.help = {
        name: "donate",
        desclist: "Shows information on how to donate.",
        usage: "/donate"
    }