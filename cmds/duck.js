const Discord = require("discord.js");

module.exports.run = async (bot, message, args) => {

const embed = new Discord.RichEmbed()
.setColor("RANDOM")
.setAuthor("Floofy Bot", bot.user.avatarURL)
.setFooter(`Cute Ducky`)
.setImage("https://is.gd/cuteducky");
  message.channel.send({
    embed
  });
}

module.exports.help = {
	name: "duck",
  desclist: "Lovely image of Duck.",
  usage:"/duck"
}