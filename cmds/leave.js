const Discord = require("discord.js")
const { voice: link } = require('../index.js');

module.exports.run = async (bot, message, args) => {
    const player = link.players.get(message.guild.id);
    player.destroy();
    message.channel.send(':octagonal_sign: **Disconnected from the** \`Voice Channel\`.');
}

module.exports.help = {
    name:"leave",
    desclist:"Leave's a voice channel.",
    usage:"/leave"
  }
