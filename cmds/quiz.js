const Discord = require('discord.js');

const quiz = [
  { q: "What color is the sky?", a: ["no color", "invisible", "clear"] },
  { q: "Name a soft drink brand.", a: ["pepsi", "coke", "rc", "7up", "sprite", "mountain dew", "fanta", "coca cola", "irn bru", "dr pepper"] },
  { q: "Name a programming language.", a: ["actionscript", "coffeescript", "c", "c++", "basic", "python", "perl", "javascript", "dotnet", "lua", "crystal", "go", "d", "php", "ruby", "rust", "dart", "java", "javascript", "kotlin"] },
  { q: "Who's a good boy?", a: ["you are", "whirl"] },
  { q: "Who created me?", a: ["floof", "hoppity floof", "floofy", "brad"] },
  { q: "What programming language am I made in?", a: ["nodejs", "nodejs", "javascript",] },
  { q: "Name the seventh planet from the Sun.", a: ["uranus"] },
  { q: "Name the World's biggest island.", a: ["greenland",] },
  { q: "What's the World's longest river?", a: ["amazon", "amazon river"] },
  { q: "Name the World's largest ocean.", a: ["pacific", "pacific ocean"] },
  { q: "Name one of the three primary colors.", a: ["blue", "red", "yellow"] },
  { q: "How many colors are there in a rainbow?", a: ["7", "seven"] },
  { q: "What do you call a time span of one thousand years?", a: ["millennium"] },
  { q: "How many squares are there on a chess board?", a: ["64", "sixty four"] },
  { q: "How many degrees are found in a circle?", a: ["360", "360 degrees", "three hundred sixty"] },
  { q: "The Dewey Decimal system is used to categorize what?", a: ["books"] },
  { q: "How many points does a compass have?", a: ["32", "thirty two"] },
  { q: "How many strings does a cello have?", a: ["4", "four"] },
  { q: "How many symphonies did Beethoven compose?", a: ["9", "nine"] },
  { q: "How many lines should a limerick have?", a: ["5", "five"] },
  { q: "What is the most basic language Microsoft made?", a: ["visual basic"] },
  { q: "What is the most complicated language?", a: ["binary"] },
  { q: "'OS' computer abbreviation usually means?", a: ["operating system"] },
  { q: "Name one British city has an underground rail systems?", a: ["liverpool", "glasgow", "newcastle", "london"]},
  { q: "What is the capital city of Spain?", a: ["madrid"]},
  { q: "The beaver is the national emblem of which country?", a: ["canada"]},
  { q: "Which singer’s real name is Stefani Joanne Angelina Germanotta?", a: ["lady gaga"]},
  { q: "How many players are there in a baseball team?", a: ["9", "nine"]},
  { q: "The average human body contains how many pints of blood?", a: ["9", "nine"]},
  { q: "Hg is the chemical symbol of which element?", a: ["mercury"]},
  { q: "In Fahrenheit, at what temperature does water freeze?", a: ["32", "thirty two"]},
  { q: "Which of the planets is closest to the sun?", a: ["mercury"]},
  { q: "In the sport of Judo, what color belt follows an orange belt?", a: ["green"]},
  { q: "How many letters are there in the German alphabet?", a: ["30", "thirty"]},
  { q: "In 1960, which country became the first in the world to have a female Prime Minister?", a: ["sri lanka"]},
  { q: "Little Cuba is the nickname of which US city?", a: ["miami"]},
  { q: "Which part of your body continues to grow throughout your life?", a: ["ears", "nose", "ear", "nose and ears", "ears and nose"]},
  { q: "Do goldfish really have a memory of three seconds?", a: ["no", "nope", "nah", "nop", "noo"]},
  { q: "What is the fear of being buried alive known as?", a: ["taphophobia"]},
  { q: "Where was the fortune cookie actually invented?", a: ["america"]},
  { q: "Which animal is known to kill more people than plane crashes?", a: ["donkey", "donkies", "a donkey", "donkys"]},
  { q: "The Empire State Building is composed of how many bricks?", a: ["ten million", "10 million", "10 mill"]},
  { q: "How many eggs does a female mackerel lay at a time?", a: ["500,000", "500k"]},
  { q: "How long is a kangaroo baby, when it is born?", a: ["1 inch", "one inch"]},
  { q: "Which animal swallows large stones and uses them as ballast?", a: ["crocodile", "crocodiles"]},
  { q: "If cats are feline, and dogs, canine, then what are bears?", a: ["ursine"]},
  { q: "What is the cross between a donkey and a zebra known as?", a: ["zonkey", "zebonkey", "zebrula", "zebrass", "zedonk"]},
  { q: "Is the Great Wall of China really visible from the moon?", a: ["no", "nope", "nah"]},
  { q: "Say a sentence that contains every letter of the English Alphabet.", a: ["the quick brown fox jumps over a lazy dog"]},
  { q: "What is the dot over the letter 'i' and 'j' called?", a: ["tittle", "a tittle"]},
  { q: "What is the world's most popular first name?", a: ["muhammad"]},
  { q: "How much time, on an average, does a person spend in the toilet in their lifetime?", a: ["3 years", "three years"]},
  { q: "**FUN FACT** - In Hong Kong, a wife, who has been betrayed by her adulterous husband, is legally allowed to: kill her husband, but with bare hands! (**Type** '\`wow\`' **to skip**)", a: ["wow"]},
  { q: "How many steps would you have to take before you reach the first level of the Eiffel Tower?", a: ["300", "three hundred"]},
  { q: "What is 'milk' called in Indonesia?", a: ["susu"]}

];
const options = {
  max: 1,
  time: 30050,
  errors: ["time"],
};

module.exports.run = async (bot, message, args) => {
  
  const item = quiz[Math.floor(Math.random() * quiz.length)];
  await message.channel.send(item.q);
  try {
    const collected = await message.channel.awaitMessages(answer => item.a.includes(answer.content.toLowerCase()), options);
    const winnerMessage = collected.first();
    return message.channel.send({embed: new Discord.RichEmbed()
        .setAuthor(`Winner: ${winnerMessage.author.tag}`, winnerMessage.author.displayAvatarURL)
        .setTitle(`Correct Answer: \`${winnerMessage.content}\``)
        .setFooter(`Question: ${item.q}`)
        .setColor(message.guild.me.displayHexColor)
                                })
  } catch (_) {
    return message.channel.send({embed: new Discord.RichEmbed()
        .setAuthor('No one got the answer in time!')
        .setTitle(`Correct Answer(s): \`${item.a}\``)
        .setFooter(`Question: ${item.q}`)
                                })
  }
}
module.exports.help = {
name: "quiz"
}
module.exports.help = {
    name: "quiz",
    desclist: "Start a quiz",
    usage: "/quiz"
}