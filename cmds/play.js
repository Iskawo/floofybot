const Discord = require("discord.js")
const { voice: link } = require('../index.js');
const { Queue } = require("node-lavalink");

module.exports.run = async (bot, message, args) => {

    const queue = new Queue();
    if (!message.member.voiceChannel) {
        message.channel.send(":x: **Please join a** \`Voice Channel\` **before using /play.**");
     } else {
    link.join(message.guild.id, message.member.voiceChannel.id);

    const player = link.players.get(message.guild.id);
    const results = await link.rest.search(`ytsearch: ${args.slice(0).join(" ")}`);

    player.play(results[0].track);

    message.channel.send({embed: {
        color: 3447003,
        title: `Added to the queue\n\n**__${results[0].info.title}__**`,
        url: results[0].info.uri,
        thumbnail: {url: `https://img.youtube.com/vi/${results[0].info.identifier}/hqdefault.jpg`},
        fields: [{
            name: "Channel:",
            value: `\`\`${results[0].info.author}\`\``
          }
        ],
        timestamp: new Date(),
        footer: {
          icon_url: message.author.avatarURL,
          text: `Added by: ${message.author.username}`
             }
            }
        });
    }
}
module.exports.help = {
    name:"play",
    desclist:"Plays a song.",
    usage:"/play"
  }
