const Discord = require("discord.js");
const ms = require("ms");
const db = require("quick.db");
const send = require('quick.hook');

module.exports.run = async (bot, message, args) => {
    let reason = args.slice(1).join(' ');
    let user = (message.mentions.users.first() || message.guild.members.get(args[0]));
    let muteChannel = message.guild.channels.get(db.get(`logchannel${message.guild.id}`));
    let tomute = message.guild.member(message.mentions.users.first() || message.guild.members.get(args[0]));
    if(!tomute) return message.channel.send(":x: Please specify a \`User\`.");
    if(!message.member.hasPermission("MUTE_MEMBERS")) return message.channel.send(":x: **Missing Permission**: \`Mute Members\`.");
    if(tomute.hasPermission("ADMINISTRATOR")) return message.reply(":x: Sorry, This user can't be \`muted\`.");
    let muterole = message.guild.roles.find(`name`, "Muted");

    if(!muterole){
        try{
          muterole = await message.guild.createRole({
            name: "Muted",
            color: "#95A5A6",
            permissions:[]
          })
          message.guild.channels.forEach(async (channel, id) => {
            await channel.overwritePermissions(muterole, {
              SEND_MESSAGES: false,
              ADD_REACTIONS: false
            });
          });
        }catch(e){
          console.log(e.stack);
        }
      }

      let mutetime = args[1];
      if(!mutetime) return message.channel.send(":warning: **Error**, Please specify a \`time\`.");
      if(tomute.roles.has("name", "Muted")) message.reply(":x: Sorry, This user is already \`muted\`.")
      else {
      await(tomute.addRole(muterole.id));   
      message.channel.send(`:white_check_mark: \`${user.username}\` (\`${tomute.id}\`) **has been muted for** \`${ms(ms(mutetime))}\` :mute:`);
      muteChannel.send({embed: {
        color: 16381497,
        author: {
        name: `Floofy Mute Logs`,
        icon_url: message.author.avatarURL
        },
        title: `**Mute** issued to:`,
        description: `${tomute} (\`${tomute.id}\`) for (\`${ms(ms(mutetime))}\`)`,
         fields: [{
             name: "\n**Staff Member**:",
             value: `\n__${message.author.username}__ (\`${message.author.id}\`)\n`
         },
         {
             name: "**Reason**:",
            value: `\`${reason}\``
         }
        ],
      },
        timestamp: new Date(),
        footer: {
        icon_url: bot.avatarURL,
        text: "Warnings are not currently tracked"
      }
    });
      setTimeout(function(){
        tomute.removeRole(muterole.id);
      }, ms(mutetime))
    }
  }
module.exports.help = {
    name: "mute",
    desclist: "Mutes a user for the specified amount of time",
    usage: "mute"
}