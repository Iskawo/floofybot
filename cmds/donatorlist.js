const Discord = require('discord.js');
const fs = require("fs");
const dusers = JSON.parse(fs.readFileSync("./users.json", "utf8"));

module.exports.run = async (bot, message, args) => {
    if (message.author.id == "286509757546758156") {
        message.channel.send({embed: {
            title: "Premium Users",
            color: 0x00AE86,
            description: `\n${dusers.donator.map(donator => `<@!${donator}>`)}`,
            footer: {text: "Requested by: " + message.author.username, icon_url: message.author.avatarURL}
        }});
    } else {
        message.channel.send('\*\*:x: Only \`\`Hoppity Floof\`\` can use this command.\*\*');
    };
}
module.exports.help = {
name: "donatorlist",
desclist: "Shows the list of donators",
usage: "/donatorlist"
}