const Discord = require("discord.js");
const fs = require("fs")
const dusers = JSON.parse(fs.readFileSync("./users.json", "utf8"));
const req = require('snekfetch');

module.exports.run = async (bot, message) => {
    let messageArray = message.content.split(" ");
    let args = messageArray.slice(1);
    let link = args.join(" ")
    if (!dusers.donator.includes(message.author.id)) return message.channel.send(":x: You must **\`donate\`** to use that feature. \`/donate\`");
    if (!args[0]) return message.channel.send(`:x: \`Incorrect Usage\`. Correct usage: **/checklink <link>**`);
            if(!link) return message.channel.send(`:x: Please provide a \`Valid\` link to check`);
            


            req.get(`https://spoopy.link/api/${link}`)
            .then(res =>{
                let bod = res.body
                if(bod.chain){
                    const emd = new Discord.RichEmbed()
                    .addField(`**Floofy Link Checker**`,`\`\nLink:\` ${bod.chain[0].url}\n\n**Is the link safe**? **\`${bod.chain[0].safe}\`**\n`)
                    .setTimestamp()
                    .setColor('BFEDAF')
                    .setFooter("Floofy Link Checker")
                    message.channel.send({embed: emd})

                    if(!res.statusCode == 200) {
                        const embed = new Discord.RichEmbed()
                        .addField(`**Floofy Link Checker**`,`\n**Your link**, __\`${bod.chain[0].url}\`__, **is invalid**\n`)
                        .setTimestamp()
                        .setColor('BFEDAF')
                        .setFotter("Floofy Link Checker")
                        message.channel.send({embed: embed})
                    }
                }
            })
        }
    module.exports.help = {
        name: "checklink",
        desclist: "Check a link to see if it's safe",
        usage: "/checklink"
      }