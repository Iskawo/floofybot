const Discord = require("discord.js");
const db = require("quick.db");
const send = require('quick.hook');

module.exports.run = async (bot, message, args) => {


if (message.member.hasPermission("MANAGE_MESSAGES")) {    
  let unMuteChannel = message.guild.channels.get(db.get(`logchannel${message.guild.id}`));
  let muteRole = message.guild.roles.find("name", "Muted");
    if (!muteRole) {
        message.channel.send(`:warning: \`Muted\` **role is not configured**.`)
    }
  let member = message.mentions.members.first();
  if(!member) return message.channel.send(`:x: Please give me a valid \`User\` to unmute.`);

  if(!member.roles.find("name", "Muted")) return message.channel.send(`:x: \`${member.user.username}\` **isn't muted**`)
      
  member.removeRole(muteRole.id);
  unMuteChannel.send(`:white_check_mark: \`${user.username}\` (\`${tomute.id}\`) **has been \`unmuted\` by** \`${message.author.username}\``);
  message.channel.send(`:white_check_mark: \`${member.user.username}\` **is now** \`Unmuted\`.`)
} else {
  message.channel.send(":x: **Insufficient Permission**.");
  }
}
    module.exports.help = {
      name: "unmute",
      desclist: "Unmute a muted member",
      usage: "/unmute"
    }