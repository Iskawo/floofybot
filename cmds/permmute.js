const Discord = require("discord.js")
const ms = require("ms");
const db = require("quick.db");
const send = require('quick.hook');

module.exports.run = async (bot, message) => {

        let messageArray = message.content.split(" ");
        let args = messageArray.slice(1);
        let kickChannel = message.guild.channels.get(db.get(`logchannel${message.guild.id}`));
        let mute = message.guild.member(message.mentions.users.first() || message.guild.members.get(args[0]));
        if(!mute) return message.channel.send(":x: Please specify a \`User\`.");
        if(!message.member.hasPermission("MUTE_MEMBERS")) return message.channel.send(":x: **Missing Permission**: \`Mute Members\`.");
        if(mute.hasPermission("ADMINISTRATOR")) return message.reply(":x: Sorry, This user can't be \`muted\`.");
        let muterole = message.guild.roles.find(`name`, "Muted");
        if(!muterole) return message.channel.send("There is no \`Muted\` role")
        mute.addRole(muterole.id);
        message.channel.send(`:white_check_mark: ${mute} (\`${mute.id}\`) **has been perm muted** :mute:`);
        await mute.send(`:warning: You have been \`Permanently Muted\` in \`${message.guild.name}\`.`);
}
module.exports.help = {
    name:"permmute",
    desclist:"Permanently mutes a user.",
    usage:"/permmute"
  }
