const Discord = require("discord.js");

module.exports.run = async (bot, message, args) => {

        message.channel.send(`Found a \`bug\` or have an \`issue\` you want help with? No problem, just join this server and we'll do our best to help! :link: https://discord.gg/4K4DYM5 :link:`)
}


module.exports.help = {
	name: "support",
  desclist: "Should be used whn needing help.",
  usage:"/support"
}