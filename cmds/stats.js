const Discord = require("discord.js");

module.exports.run = async (bot, message, args) => {

 let embed = new Discord.RichEmbed()
    .setAuthor(message.author.username)
    .setDescription(`\n**The bot is currently in** \`${bot.guilds.size}\` **guilds, with** \`${bot.channels.size}\` **channels and** \`${bot.guilds.map(g => g.memberCount).reduce((f, l) => f + l)}\` **users.**\n** **\n\`Floofy Bot\`:tm: ** ** ** ** • ** ** ** ** Hoppity Floof#6172`)
    .setColor("#9B59B6");
  message.channel.sendEmbed(embed);
}


module.exports.help = {
	name: "stats",
  desclist: "Used to see the statistics of the bot - How many guilds/users/members we have",
  usage:"/stats"
}
