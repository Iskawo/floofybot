const Discord = require("discord.js");

module.exports.run = async (bot, message, args) => {
  if(!message.member.hasPermission("MANAGE_MEMBERS")) return message.reply("Sorry pal, you can't do that.");
  let rMember = message.guild.member(message.mentions.users.first()) || message.guild.members.get(args[0]);
  if(!rMember) return message.channel.send(`:x: **Please enter a valid** \`User\`.`);
  let role = args.join(" ").slice(22);
  if(!role) return message.channel.send(`:x: **Please specify a valid** \`Role\`.`);
  let gRole = message.guild.roles.find(`name`, role);
  if(!gRole) return message.channel.send(`:x: **Unable to find the specified** \`Role\`.`);

  if(!rMember.roles.has(gRole.id)) return message.channel.send(`:x: **The user doesn't have that \`Role\`.`);
  await(rMember.removeRole(gRole.id));

  try{
    await rMember.send(`:warning: You were removed from the role \`${gRole.name}\` in ${message.guild.name}.`)
  }catch(e){
    message.channel.send(`:warning: We removed \`${gRole.name}\` from \`<@${rMember.id}>\`. We unfortunately tried to DM them, but their DMs are locked.`)
  }
}

module.exports.help = {
  name: "removerole",
  desclist:"Removes a role from a user",
  usage:"/removerole"
}