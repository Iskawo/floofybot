const Discord = require('discord.js')
var figlet = require('figlet');

module.exports.run = (client, message, args, tools) => {
  
  var maxLen = 14 // You can modify the max characters here
  
  if(args.join(' ').length > maxLen) return message.channel.send(`:x: **Too long! Try something with maximum of** \`14\` **characters.**`) 
  
  if(!args[0]) return message.channel.send(`:x: **Please send something to turn into ascii:** \`/ascii <YourWordHere>\``);
  
  figlet(`${args.join(' ')}`, function(err, data) {
      if (err) {
          console.log('Something went wrong...');
          console.dir(err);
          return;
      }

      message.channel.send(`${data}`, {code: 'AsciiArt'});
  });


}
module.exports.help = {
    name: "ascii",
    desclist: "Turn your words into Ascii",
    usage: "/ascii <word>"
}