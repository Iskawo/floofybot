const request = require("request")
module.exports.run = async (bot, message) => { 
  if (!message.channel.nsfw) return message.channel.send(":underage: NSFW Command. Please switch to **\`NSFW channel\`** in order to use this command.")
  let messageArray = message.content.split(" ");
  let args = messageArray.slice(1);
  let search = args[0];
  let resultNum = args[1];
  if(!search)
  {
      return message.channel.send(`:x: **Please provide a valid** **\`Query\`**.`);
  }
  const baseUrl = "http://api.urbandictionary.com/v0/define?term=";
  const theUrl = baseUrl + search;
  request({
    url: theUrl,
    json: true,
  }, (error, response, body) => {
    if (!resultNum) {
      resultNum = 0;
    } else if (resultNum > 1) {
      resultNum -= 1;
    }
    const result = body.list[resultNum];
    if (result) {
      const definition = [
        `**Word** ${search}`,
        "",
        `**Definition** ${resultNum += 1} out of ${body.list.length}\n${result.definition}`,
        "",
        `**Example**\n${result.example}`,
        `<${result.permalink}>`,
      ];
      message.channel.sendMessage(definition).catch(err => client.funcs.log(err.stack, "error"));
    } else {
      message.channel.sendMessage("No entry found.").catch(err => client.funcs.log(err.stack, "error"));
    }
  });
}

module.exports.help = {
    name: 'urban',
    desclist: 'Returns an Urban Dictionary definition for a specified search term.',
    usage: '/urban [word]'
  }