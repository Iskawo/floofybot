const Discord = require("discord.js")
const { voice: link } = require('../index.js');

module.exports.run = async (bot, message, args) => {
    const player = link.players.get(message.guild.id);
    player.resume();
    message.channel.send(':satellite: **Resumed!**');
}

module.exports.help = {
    name:"resume",
    desclist:"Resume's a song.",
    usage:"/resume"
  }
