const Discord = require('discord.js');
const config = require('../botconfig.json');

module.exports.run = (bot, message, args) => {
    if (message.content.startsWith(config.prefix + 'createrole')) {
        const name = args.slice(0).join(" ");
        if(!message.member.hasPermission("MANAGE_ROLES")) return message.channel.send(":x: **Missing Permission**: \`Manage Roles\`.");
        if (!name) {
            message.channel.send(`:x: __**Incorrect Usage**__. **Correct Usage:** \`/createrole <rolename>\``)
        } else {
            message.guild.createRole({
                name: name,
            });
            message.channel.send(`\*\*:white_check_mark: **Success!** __Role__: \`\`${name}\`\` has been __created__\*\*`);
        }; 
    };
}
module.exports.help = {
    name: "createrole",
    desclist: "Creates a role in the server",
    usage: "/createrole <rolename>"
}