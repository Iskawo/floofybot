const Discord = require("discord.js")
const { voice: link } = require('../index.js');

module.exports.run = async (bot, message, args) => {
    const player = link.players.get(message.guild.id);
    player.pause();
    message.channel.send(':pause_button: **Paused!**');
}

module.exports.help = {
    name:"pause",
    desclist:"Pauses's a song.",
    usage:"/pause"
  }
