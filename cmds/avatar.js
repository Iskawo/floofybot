const Discord = require("discord.js");
module.exports.run = (client, message) => {
    let embed = new Discord.RichEmbed()
    .setColor("RANDOM")
    .setImage(message.author.avatarURL);
    message.channel.send(embed)
    }
module.exports.help = {
    name: "avatar",   
    description: "Sends your avatar's URL.",
    usage: "/avatar"
};