const Discord = require("discord.js");
module.exports.run = (client, message, args) => {
    let numArray = args.map(n => parseInt(n));
    let total = numArray.reduce( (p, c) => p+c);

    message.channel.send(total);
};
module.exports.help = {
    name: "add",
    desclist: "Adds numbers together",
    usage: "/add [num1] [num2] [num3] {etc}"
};